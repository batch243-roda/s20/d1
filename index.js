// console.log("Last session of the week!");

// [section] While Loop

// A while loop takes in an expression/condition.
// Expressions are any unit of code that can be evaluated to a value.
// if the condition evaluates to be truem the statements inside the code block will be executed.
// A loob will iterate a certain number of times until an expression /condition is met.
// Iteration is the term given to the repitition of statements
/*
	Syntax:
	while(expression/condition){
		statements;
		iteration;
	}
*/

/*let count = 0;
// While the value of count is not equal to zero
while(count!==0){
	// The current value of count is printed out.
	console.log("While: " + count);

	// decrement
	count--;
	console.log("Value of count after the Iteration: " +count);
	// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0.
	// Loops occcupy a significant amount of memory space in our devices.
	// Make sure that expressions/conditions in loops have theur corresponding increment/ decrement operators to stop the loop
	// Forgetting to include this in loops will make our applications run an infinite loop.
	// After running the script, if a slow response from the browser uis experienced or an infinite loop is see seen in the console quickly close the applicatio/tab/browser to avoid this.
}*/


// [section] do while Loop
/*
		- a do-while loop works a lot like  the while loop. but unlike while loops, do-while loops guarantee that the code will be executed at least once.
		Syntax:
		do{
			statement;
			iteration;
		}
		while(expression/condition)
*/

/*let number = Number(prompt("Give me a number."));

do{
	console.log("Do while: " + number);

	number++;
}while(number < 10);*/

// [Section] For loop
/*
	-A for loop is more flexible than while and do-while. 
	It consists of 3 parts:
	1. The "initialization" value that will track the progression of the loop.
	2. The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
	3. The "finalExpression" indicates how to advance the loop.

	Syntax:
	 for(initialization; expression/condition; finalExpression){
		statement/statements;
	 }
*/

	/*
		-we will create a loop that will start from 0 and end at 20
		-Every iteration of the loop, the value of count will be checked if it is equal or less than 20
		-if the value of count is less than or equal to 20 the statement inside of the loop will execute.
		-The value of count will be incremented by one for each iteration
	*/

	/*for(let count = 0; count <= 20; count++){
		console.log("The current value of count is: " + count);
	}*/
				
// let myString = "Christopher";
	// characters in strings may be counte using the .length property
	// Strings are special compare to other data types, that  it has access to functions and other pieces of information.

	// console.log(myString.length);

	// Accessing characters of a string.
	// Individual characters of a string may be accesses using it's index number.
	// The first character in a string corresponds to 0, the next is 1  upt to the nth/last character.

	/*console.log(myString[0]);
	console.log(myString[1]);
	console.log(myString[2]);
	console.log(myString[3]);*/

	/*for(let i = 0; i < myString.length; i+=2){
		console.log(myString[i]);
	}

	let numA = 15;

	for(let i = 0; i <=10; i++){
		 let exponential = numA**i

		console.log(exponential);
	}*/


	// Create a string named "myName" with value of "Alex"
	let myName = "Christopher";


	/*
		Create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is  vowel
		*/

	for(let i = 0; i < myName.length; i++){
		if(myName[i].toLowerCase() === "a" ||
			myName[i].toLowerCase() === "e" ||
			myName[i].toLowerCase() === "i" ||
			myName[i].toLowerCase() === "o" ||
			myName[i].toLowerCase() === "u"){

			console.log(3);
		}
		else{
			console.log(myName[i]);
		}
	}







